# Probabilistic Model Checking

This repository contains *unofficial* notes for Probabilistic Model Checking in LaTeX, as taught in Michaelmas Term 2020 by Professor Alessandro Abate at Oxford. These may contain errors, inconsistencies, and/or be incomplete, as a fair warning, that said, I hope they prove useful.