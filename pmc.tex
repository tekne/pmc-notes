\documentclass{article}
\usepackage[margin=1.5in]{geometry}

\usepackage{pmc}
\addbibresource{references.bib}

\title{Probabilistic Model Checking Notes}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

\begin{center}
    These are *unofficial* notes for Probabilistic Model Checking in LaTeX, as taught in Michaelmas Term 2020 by Professor Alessandro Abate at Oxford. These may contain errors, inconsistencies, and/or be incomplete, as a fair warning, that said, I hope they prove useful. Note that these loosely follow the slides posted, but have been modified by the author.
\end{center}

\tableofcontents

\newpage

\section{Introduction}

\textbf{Probabilistic model checking} is a technique in the field of \textbf{formal verification} used for analyzing systems having probabilistic behaviour. Formal verification, here, refers to the application of rigorous mathematical techniques to verify properties (such as correctness) of computerized systems.

\subsection{Formal Verification}

In the conventional software engineering process, given a set of informal requirements (e.g. ``the code shouldn't crash''), we, after programming directly in a programming language, \textbf{validate} these requirements via testing and code walkthroughs (see Figure \ref{fig:convsoft}). For example, we may work out a variety of use cases of the software, and then run it through those use cases through unit and integration testing, verifying between steps that some desired properties hold. The issue with this approach is simple: \textbf{finding errors does not prove their absence}, as if anything, scientifically, it confirms they are present!

\begin{figure}[h]
    \begin{center}
        \begin{tikzcd}
            \text{Informal requirements} \arrow[rrr, "\textit{Validation}"]
            &&& \text{System}
        \end{tikzcd}
        \caption{The conventional software development process}
        \label{fig:convsoft}
    \end{center}
\end{figure}

For critical systems, we can amend this process by introducing formal verification. To do this, we must first \textbf{formalise} our informal requirements into a formal specification. We can then \textbf{verify}, using mathematical proof, that a \textbf{model} obeys our formal specification. Such a model can be \textbf{abstracted} from a \textbf{system}, or, vice-versa, a model can be \textbf{refined} into a system. This paradigm in fact extends beyond software and can be applied to a large variety of engineering problems. The key here is that while it is still possible to make mistakes, \textit{if} our verification and abstraction/refinement are correct, \textit{then} the only place we could have made a mistake is in formally specifying our requirements. That is, like in the classic story of the genie, we were \textit{technically} given what we wished for. This, however, dramatically reduces the potential surface area for mistakes, and many encouraging properties (e.g. the system does not crash) can be often encoded relatively reliably.

\begin{figure}[h]
    \begin{center}
        \begin{tikzcd}
            \text{Formal specification}
            \arrow[rrr, "\textit{Verification}"] &&&
            \text{Model}
            \arrow[d, shift left, "\textit{Refine}"] \\
            \text{Informal requirements}
            \arrow[u, "\textit{Formalize}"] &&&
            \text{System}
            \arrow[u, shift left, "\textit{Abstract}"]
        \end{tikzcd}
        \caption{The rigorous software development process}
        \label{fig:rigsoft}
    \end{center}
\end{figure}

Now, one may question the necessity of such rigor when designing software, especially considering the existence of many successful large-scale software systems which were mostly developed conventionally, e.g. e-commerce, supply chain management, and online libraries. Yet, emerging technologies, such as self-driving cars and medical sensors, offer far greater complexity and failure costs. Furthermore, we don't have to look too far into the past to see that even today, learning from mistakes in software can be \textit{very} costly. As a short reel of highlights, consider:
\begin{itemize}

    \item The ESA (European Space Agency) Ariane 5 launcher, which self-destructed about 40 seconds after it's maiden flight began on June 4, 1996, due to an uncaught exception caused by numerical overflow when converting a 64-bit floating point to a 16-bit signed integer \cite{ariane5}.

    \item The 710 deaths associated with problems with infusion pumps over the last five years \cite{infusionpumps}.

    \item The 23 recalls of defective pacemaker devises during the first half of 2010, of which 6 were due to software defects \cite{pacemaker}.

    \item The \$3 trillions in losses caused by ``cybercrime'' in 2015, projected to rise to \$6 trillion in 2021 \cite{cybercrime}.

\end{itemize}

In general, these stories all have something in common: programmable computing devices directly caused a failure due to a programming error. In short, we can see that software is critical for safety, for business, and for performance, and the costs incurred by errors are not always merely financial. Thankfully, with rigorous software engineering, these failures may be avoided.

\TODO{Model checking, etc}

\newpage

\section{Probability Basics}

From the frequentist point of view, probability represents ``on average'' how often something will occur, whereas from the Bayesian point of view probability represents the certainty of our belief something will occur. Either way, probability gives us a way to precisely quantify \textit{uncertainty}.

To do so, we introduce the notion of a \textbf{probability space}. Informally, given a set of all possible \textbf{outcomes}, the \textbf{sample space} \(\Omega\), we want to determine the probability of \textbf{events}, which are particular sets of outcomes. Just from this informal definition, we can already work out the following:

\begin{itemize}

    \item If we toss a coin, the set of \textbf{outcomes} is given by \(\Omega = \{\mathtt{heads}, \mathtt{tails}\}\). Possible events include:
          \begin{itemize}

              \item Getting heads, \(\{\mathtt{heads}\}\), or getting tails \(\{\mathtt{tails}\}\), which both have probability \(1/2\).

              \item Getting either heads or tails, \(\{\mathtt{heads}, \mathtt{tails}\}\), which has probability \(1\).

              \item Getting neither heads nor tails, \(\{\}\), which has probability \(0\).

          \end{itemize}

    \item If we toss two coins, the set of outcomes is given by
          \(
          \Omega = \{\mathtt{heads}, \mathtt{tails}\}^2
          \),
          the set of pairs of the outcome of the first flip and the outcome of the second flip. An example of an event could then be, e.g., ``at least one coin has heads,'' which would be given by the set
          \[
              \{
              (\mathtt{tails}, \mathtt{tails}),
              (\mathtt{tails}, \mathtt{heads}),
              (\mathtt{heads}, \mathtt{heads})
              \} \subset \Omega
          \]
          having probability \(3/4\).

    \item If we toss a coin infinitely often, the set of outcomes is given by the \textit{infinite} set \(\Omega = \nats \to \{\mathtt{heads}, \mathtt{tails}\}\). An event could then be, e.g., that we get heads in the first three throws, which would be given by the set
    \[
        \{
            o \in \Omega : \exists n < 3, o(n) = \mathtt{heads}
        \} \subset \Omega
    \]
    having probability \(1 - \frac{1}{8} = \frac{7}{8}\)

\end{itemize}

If we write the set of events as \(\mc{F} \subseteq \mc{P}(\Omega)\), we also want to be able to, given events \(A, B \in \mc{F}\), reason about the probability of events like ``not \(A\)'' (\(A^c = \Omega \setminus A\)), ``\(A\) or \(B\)'' (\(A \cup B\)) and ``\(A\) and \(B\).'' Denoting the probability of an event \(A\) by \(\Pr(A)\), we then want common-sense properties of probability to hold, e.g.

\begin{itemize}

    \item \(\Pr(\Omega) = 1\)

    \item \(\Pr(A^c) = 1 - \Pr(A)\)
    
    \item \(A \cap B = \varnothing \implies \Pr(A \cup B) = \Pr(A) + \Pr(B)\), and therefore in general
    \begin{equation}
        \Pr(A \cup B) = \Pr(A) + \Pr(B) - \Pr(A \cap B)
    \end{equation}

\end{itemize}
We will also want to consider (countably) infinite collections of events: i.e. if \(A_1, A_2, ...\) are events, then we want \(A = \bigcup_iA_i\) to be an event and, similarly, we want
\begin{equation}
    \Pr(A) \leq \sum_i\Pr(A_i), \qquad \forall i, j, A_i \cap A_j = \varnothing \implies \Pr(A) = \sum_i\Pr(A_i)
\end{equation}

With this in mind, we're now ready to give a formal definition to probability spaces in general:

\begin{definition}[\(\sigma\)-algebra]
    Fix an arbitrary nonempty set \(\Omega\). We say that a family of subsets \(\mc{F} \subseteq \mc{P}(\Omega)\) is a \textbf{\(\sigma\)-algebra} over \(\Omega\) if
    \begin{enumerate}
        
        \item \(\forall A \in \mc{F}, \Omega \setminus A \in \mc{F}\)
        
        \item \(\forall i \in \mc{N}, A_i \in \mc{F} \implies \bigcup_iA_i \in \mc{F}\)
        
        \item \(\varnothing \in \mc{F}\)

    \end{enumerate}
\end{definition}
In particular, this implies that \(\Omega = \Omega \setminus \varnothing \in \mc{F}\), and that \(\mc{F}\) is closed under countable intersections (which are the complement of countable unions) as well.

We also note the following theorem
\begin{theorem}
    Given any collection of sets \(\mc{A} \subseteq \mc{P}(\Omega)\), there is a unique smallest \(\sigma\)-algebra containing \(\mc{A}\).
\end{theorem}

\begin{definition}[Measure]
    Given a nonempty set \(\Omega\) and a \(\sigma\)-algebra \(\mc{F}\) over it, we say a function \(\mu: \mc{F} \to [0, \infty]\) is a \textbf{measure} if it satisfies
    \begin{itemize}
        \item \(\mu(\varnothing) = 0\)
        \item For any countable family \(A_i \in \mc{F}\),
        \begin{equation}
            \forall i, j, A_i \cap A_j = \varnothing \implies \mu\left(\bigcup_iA_i\right) = \sum_i\mu(A_i)
        \end{equation}
    \end{itemize}
\end{definition}

In particular, this satisfies most of the properties described above. If we additionally assert that \(\mu(\Omega) = 1\), we get a \textbf{probability measure}, which satisfies all the desired properties. We can then complete our definition as follows:

\begin{definition}[Probability Space]
    A \textbf{probability space} \((\Omega, \mc{F}, \Pr)\) is an arbitrary set, the \textbf{sample space}, \(\Omega\), equipped with a \(\sigma\)-algebra \(\mc{F}\), the set of \textbf{events}, and a probability measure \(\Pr\) over this algebra.
\end{definition}

The reason why we can't just consider \textit{any} subset \(\Omega\) to constitute an event is due to measure theory: in particular, it would forbid us from properly being able to construct probabilities on certain infinite sample spaces like \(\reals\), due to the existence of ``non measurable sets'' (assuming the Axiom of Choice, that is). This is why we must fix a particular \(\sigma\)-algebra of events compatible with our probability measure.

As an example of this more formal definition, for example, we can fix a sample space \(\Omega = \{1, 2, 3\}\), and take the trivial \(\sigma\)-algebra \(\mc{P}(\Omega)\) over it. We may then define the probability measure
\begin{equation}
    \Pr(1) = \Pr(2) = \Pr(3) = \frac{1}{3}
\end{equation}
which allows us to compute
\begin{equation}
\Pr(\{1, 2\}) = \Pr(1) + \Pr(2) = \frac{1}{3} + \frac{1}{3} = \frac{2}{3}
\end{equation}

For an infinite example, take \(\Omega = \nats\), and consider the \(\sigma\)-algebra
\begin{equation}
    \mc{F} = \{\varnothing, 2\nats, 2\nats + 1, \nats\}
\end{equation}
where \(2\nats\) denotes the even numbers and \(2\nats + 1\) the odd numbers. We can confirm this is a \(\sigma\)-algebra as \(\nats \setminus 2\nats = 2\nats + 1\). Think of this as the statement ``the possible events are getting nothing, getting an odd number, getting an even number, or getting any number,'' which is distinct from the possible \textit{outcomes}, which correspond to the (infinite) collection of all natural numbers. We may then define a probability measure by, e.g.
\begin{equation}
    \Pr(2\nats) = \Pr(2\nats + 1) = \frac{1}{2}
\end{equation}

\newpage

\section{Discrete-time Markov Chains}

\TODO{this}

\newpage

\addcontentsline{toc}{section}{References}
\printbibliography

\end{document}